//
//  TimePoint.swift
//  Hair Stylist
//
//  Created by Salvatore Serafino on 04/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import Foundation

struct TimePoint: Decodable {
    let id: Int!
    let point: Point!
    let progDay: Int!
    let ap1: String!
    let ap2: String!
    let ap3: String!
    let c1: String!
    let c2: String!
    let c3: String!
    let closed: Bool!
    
    init(id: Int,
        point: Point,
        progDay: Int,
        ap1: String,
        ap2: String,
        ap3: String,
        c1: String,
        c2: String,
        c3: String,
        closed: Bool
    ) {
        self.id = id
        self.point  = point
        self.progDay = progDay
        self.ap1 = ap1
        self.ap2 = ap2
        self.ap3 = ap3
        self.c1 = c1
        self.c2 = c2
        self.c3 = c3
        self.closed = closed
    }
    
    enum CodingKeys: String, CodingKey {
      case id = "id"
      case point  = "point"
      case progDay = "progDay"
      case ap1 = "ap1"
      case ap2 = "ap2"
      case ap3 = "ap3"
      case c1 = "c1"
      case c2 = "c2"
      case c3 = "c3"
      case closed = "closed"
    }
}



