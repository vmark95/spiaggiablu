//
//  CoordinateReq.swift
//  Hair Stylist
//
//  Created by Salvatore Serafino on 04/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import Foundation


struct CoordinateReq: Decodable {
    let name: String!
    let lat: Double!
    let lng: Double!
    let size: Int!
    let page: Int!
    let maxDistance: Int!

    init(name: String,
        lat: Double,
        lng: Double,
        size: Int,
        page: Int,
        maxDistance: Int
    ) {
        self.name = name
        self.lat = lat
        self.lng = lng
        self.size = size
        self.page = page
        self.maxDistance = maxDistance
    }
    func toDict() -> [String: Any] {
        return ["name": self.name ?? "", "lat": self.lat ?? 0.0, "lng": self.lng ?? 0.0, "size": self.size ?? 0, "page": self.page ?? 0, "maxDistance": self.maxDistance ?? 0]
    }
}
