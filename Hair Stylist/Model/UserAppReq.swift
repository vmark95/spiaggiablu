//
//  UserAppReq.swift
//  Hair Stylist
//
//  Created by Salvatore Serafino on 04/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import Foundation


struct UserAppReq: Decodable {
    let uid: String!
    let tokenMobile: String!
    let username: String!
    
    init(uid: String,
        tokenMobile: String,
        username: String
    ) {
        self.uid = uid
        self.tokenMobile = tokenMobile
        self.username = username
    }
    
    func toDict() -> [String: String] {
        return ["uid": uid ?? "", "tokenMobile": tokenMobile ?? "", "username": username ?? ""]
    }
}

