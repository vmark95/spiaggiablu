//
//  Point.swift
//  Hair Stylist
//
//  Created by Salvatore Serafino on 04/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import Foundation

struct Point: Decodable {
    let id: Int!
    let name: String!
    let description: String!
    let piva: String!
    let coduniv: String!
    let address: String!
    let latitude: String!
    let longitude: String!
    let phone: String!
    let mail: String!
    let site: String!
    let image: String!
    let enabled: Bool!
    let dateCreate: String!
    let restUrl: String!
    let pinAdmin: Int!
    let pinStaff: Int!
    let timePoints: [TimePoint]!
    let staffSet: [User]!
    let favoriteSet: [User]!
    let meter: Int!
    var service: [Service]!
    
    init(id: Int,
        name: String,
        description: String,
        piva: String,
        coduniv: String,
        address: String,
        latitude: String,
        longitude: String,
        phone: String,
        mail: String,
        site: String,
        image: String,
        enabled: Bool,
        dateCreate: String,
        restUrl: String,
        pinAdmin: Int,
        pinStaff: Int,
        timePoints: [TimePoint],
        staffSet: [User],
        favoriteSet: [User],
        meter: Int,
        service: [Service]
    ) {
        self.id = id
        self.name = name
        self.description = description
        self.piva = piva
        self.coduniv = coduniv
        self.address = address
        self.latitude = latitude
        self.longitude = longitude
        self.phone = phone
        self.mail = mail
        self.site = site
        self.image = image
        self.enabled = enabled
        self.dateCreate = dateCreate
        self.restUrl = restUrl
        self.pinAdmin = pinAdmin
        self.pinStaff = pinStaff
        self.timePoints = timePoints
        self.staffSet = staffSet
        self.favoriteSet = favoriteSet
        self.meter = meter
        self.service = service
        
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case description = "description"
        case piva = "piva"
        case coduniv = "coduniv"
        case address = "address"
        case latitude = "latitude"
        case longitude = "longitude"
        case phone = "phone"
        case mail = "mail"
        case site = "site"
        case image = "image"
        case enabled = "enabled"
        case dateCreate = "dateCreate"
        case restUrl = "restUrl"
        case pinAdmin = "pinAdmin"
        case pinStaff = "pinStaff"
        case timePoints = "timePoints"
        case staffSet = "staffSet"
        case favoriteSet = "favoriteSet"
        case meter = "meter"
        case service = "service"
    }
}



