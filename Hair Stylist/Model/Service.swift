//
//  Service.swift
//  Hair Stylist
//
//  Created by Salvatore Serafino on 04/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import Foundation

struct Service: Decodable {
    var id: Int!
    var description: String!
    var minute: Int!
    var priority: Int!
    var price: Double!
    var enabled: Bool!
    var staffDetails: [StaffDetail]!
    var idStaffSelected: Int!
    var checked: Bool? = false
    
    
    init(id: Int,
        description: String,
        minute: Int,
        priority: Int,
        price: Double,
        enabled: Bool,
        staffDetails: [StaffDetail],
        idStaffSelected: Int
    ) {
        self.id = id
        self.description = description
        self.minute = minute
        self.priority = priority
        self.price = price
        self.enabled = enabled
        self.staffDetails = staffDetails
        self.idStaffSelected = idStaffSelected
    }
    
    enum CodingKeys: String, CodingKey {
      case id = "id"
      case description = "description"
      case minute = "minute"
      case priority = "priority"
      case price = "price"
      case enabled = "enabled"
      case staffDetails = "staffDetails"
      case idStaffSelected = "idStaffSelected"
    }
}


