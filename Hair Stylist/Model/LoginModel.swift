//
//  LoginModel.swift
//  Hair Stylist
//
//  Created by Marco Vastolo on 21/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import Foundation

struct LoginModel: Codable {
    var uid: String!
    
    
    enum CodingKeys: String, CodingKey {
           
          case uid = "uid"
           
          }
}
