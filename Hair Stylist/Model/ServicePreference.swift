//
//  ServicePreference.swift
//  Hair Stylist
//
//  Created by Marco Vastolo on 20/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import Foundation


struct ServicePreference: Codable {
    var service: Int?
    var staffId: Int?
    var priority: Int?
    var description: String?
    var numSlot: Int?
    var start: String?
    var end: String?
    
    
    enum CodingKeys: String, CodingKey {
        case service = "service"
        case staffId = "staffId"
        case priority = "priority"
        case description = "description"
        case numSlot = "numSlot"
        case start = "start"
        case end = "end"
        
       }
    
    /*init(service: Int,
         staffId: Int,
         priority: Int,
         description: String,
         numSlot: Int,
         start: String,
         end: String) {
        
        self.service = service
        self.staffId = staffId
        self.priority = priority
        self.description = description
        self.numSlot = numSlot
        self.start = start
        self.end = end
    }
*/
}
