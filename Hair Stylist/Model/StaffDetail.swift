//
//  StaffDetail.swift
//  Hair Stylist
//
//  Created by Salvatore Serafino on 04/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import Foundation

struct StaffDetail: Decodable {
    var id: Int!
    let uid: String!
    var fullname: String!
    let image: String!
    let tokenMobile: String!
    let enabled: Bool!
    
    
    init(id: Int,
        uid: String,
        fullname: String,
        image: String,
        tokenMobile: String,
        enabled: Bool
    ) {
        self.id = id
        self.uid = uid
        self.fullname = fullname
        self.image = image
        self.tokenMobile = tokenMobile
        self.enabled = enabled
    }
    
    enum CodingKeys: String, CodingKey {
      case id = "id"
      case uid = "uid"
      case fullname = "fullname"
      case image = "image"
      case tokenMobile = "tokenMobile"
      case enabled = "enabled"
    }
}

