//
//  OKPrenotazioneViewController.swift
//  Hair Stylist
//
//  Created by Marco Vastolo on 22/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI

class OKPrenotazioneViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var orario: UILabel!
    @IBOutlet weak var data: UILabel!
    @IBOutlet weak var nomeBarbiere: UILabel!
    @IBOutlet weak var aggiungiCalendarioOutlet: UIButton!
    @IBOutlet weak var tableViewServizi: UITableView!
    @IBOutlet weak var codiceAdesione: UILabel!
    
    var dateFormatter = DateFormatter()
    var avReq: AvailabilityReq!
    var point: Point!
    var user: User!
    var ora = ""
    var dataa = Date()
    var appointmentReq: AppointmentReq!
    var dataSelected = Date()
    var servizi = [String]()
    var startDate = Date()
    var endDate = Date()
    var dataHHHHH = Date()
    var startDateString = ""
    var endDateString = ""
    var arrat = [String]()
    var rcode = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        codiceAdesione.text = "Codice di prenotazione: \(rcode)"
        
        dateFormatter.dateFormat = "dd MMMM yyyy"
        dateFormatter.locale = Locale(identifier: "it_IT")
        data.text = dateFormatter.string(from: dataSelected)
        nomeBarbiere.text = point.name
        tableViewServizi.delegate = self
        tableViewServizi.dataSource = self
       
        tableViewServizi.register(UINib(nibName: "ServiziPrenotatiTableViewCell", bundle: nil), forCellReuseIdentifier: "serviziPrenotati")
        tableViewServizi.tableFooterView = UIView()
        if appointmentReq.servicePreferences != nil {
            for av in appointmentReq.servicePreferences! {
                arrat.append(recuperoDescrizioneServizi(id: av.service!))
                
            }
        }
        
       
    
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointmentReq.servicePreferences!.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "serviziPrenotati") as! ServiziPrenotatiTableViewCell
        cell.serviziPrenotati.text = "\(recuperoDescrizioneServizi(id: appointmentReq.servicePreferences![indexPath.row].service!))"
    
        return cell
    }
    

    @IBAction func tornaHome(_ sender: Any) {
        self.performSegue(withIdentifier: "indietro", sender: self)
    }
    
    
    
    
    func recuperoDescrizioneServizi(id: Int) -> String {
        var indexService = point.service.firstIndex(where: { $0.id == id })!
        var descrizioneServizio = point.service[indexService].description
        //var indexServiceStaff = point.service[indexService].staffDetails.firstIndex(where: { $0.id == idStaff })!
       // var nomeStaff = point.service[indexService].staffDetails[indexServiceStaff].fullname
        return "\(descrizioneServizio!)"
        
        
    }
    
    
    
    
}




