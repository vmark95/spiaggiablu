//
//  ListaPrenotazioniViewController.swift
//  SpiaggiaBlu
//
//  Created by Marco Vastolo on 01/06/2020.
//  Copyright © 2020 Gecotech Srl. All rights reserved.
//

import UIKit
import Alamofire

class ListaPrenotazioniViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var nomeLido: UILabel!
    @IBOutlet weak var inData: UILabel!
    @IBOutlet weak var numVisitors: UILabel!
    @IBOutlet weak var prenotazioniTableView: UITableView!
    @IBOutlet weak var alertTableView: UITableView!
    @IBOutlet weak var segmented: UISegmentedControl!
    @IBOutlet weak var alertImage: UIImageView!
    @IBOutlet weak var alertView: UIView!
    
    var prenotazioneResponse: PrenotazioneResponse!
    var dateFormatter = DateFormatter()
    var prenotazioniPassate = [Prenotazione]()
    var prenotazioniNuove = [Prenotazione]()
    var user: User!
    var point: Point!
    let arrayPrimo = ["uno", "due", "tre"]
    let arraySecondo = ["1", "2", "3", "4", "5"]
    var prenotazioniResponse = [PrenotazioneResponse]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertTableView.dataSource = nil
        alertTableView.delegate = self
        alertTableView.tag = 2
        alertTableView.tableFooterView = UIView()
        alertImage.layer.cornerRadius = 15
        prenotazioniTableView.tableFooterView = UIView()
        prenotazioniTableView.tag = 1
        chiamataPrenotazioniNuove()
        chiamataPrenotazioniPassate()
        prenotazioniTableView.register(UINib(nibName: "ListaPrenotazioniCellaTableViewCell", bundle: nil), forCellReuseIdentifier: "listaPrenotazioniCell")
        
        alertTableView.register(UINib(nibName: "PrenotazioniEffettuateTableViewCell", bundle: nil), forCellReuseIdentifier: "PrenotazioniEffettuateCell")
        
        segmented.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)
        
        segmented.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: UIControl.State.normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        chiamataPrenotazioniNuove()
        chiamataPrenotazioniPassate()
    }
    
    @IBAction func chiudi(_ sender: Any) {
        alertView.isHidden = true
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(prenotazioniNuove[indexPath.row].dateReservation)
       
        if segmented.selectedSegmentIndex == 0 {
        
            chiamataAlert(point: prenotazioniNuove[indexPath.row].point, rcode: prenotazioniNuove[indexPath.row].rCode)
      
                nomeLido.text = prenotazioniNuove[indexPath.row].point.name
            
          
        } else {
            chiamataAlert(point: prenotazioniPassate[indexPath.row].point, rcode: prenotazioniPassate[indexPath.row].rCode)
            nomeLido.text = prenotazioniPassate[indexPath.row].point.name
        }
        DispatchQueue.main.async {
            self.alertView.isHidden = false
        }
      
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
        if segmented.selectedSegmentIndex == 0 {
            return prenotazioniNuove.count
        } else {
            return prenotazioniPassate.count
        }
        } else if tableView.tag == 2 {
            if prenotazioneResponse.serviceResponses!.count > 0 {
                return prenotazioneResponse.serviceResponses!.count
            } else {
                return 0
            }
        } else {
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listaPrenotazioniCell") as! ListaPrenotazioniCellaTableViewCell
        cell.selectionStyle = .none
        if segmented.selectedSegmentIndex == 0 {
            cell.nomeLido.text = prenotazioniNuove[indexPath.row].point.name
            //cell.data.text = prenotazioniNuove[indexPath.row].dateReservation
            cell.data.text = createDataString(dataString: prenotazioniNuove[indexPath.row].dateReservation)
            cell.codicePrenotazione.text = "Codice Prenotazione : \(prenotazioniNuove[indexPath.row].rCode)"
            
        } else {
            cell.nomeLido.text = prenotazioniPassate[indexPath.row].point.name
            //cell.data.text = prenotazioniPassate[indexPath.row].dateReservation
            cell.data.text = createDataString(dataString: prenotazioniPassate[indexPath.row].dateReservation)
            cell.codicePrenotazione.text = "Codice Prenotazione: \(prenotazioniPassate[indexPath.row].rCode)"
        }
        return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PrenotazioniEffettuateCell") as! PrenotazioniEffettuateTableViewCell
            cell.labelDescrizione.text = "n.\(prenotazioneResponse.serviceResponses![indexPath.row].quantita!) - \(prenotazioneResponse.serviceResponses![indexPath.row].description!)"
            return cell
        }
    }
    

    @IBAction func segmentedAction(_ sender: Any) {
        prenotazioniTableView.reloadData()
    }
    

    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    func chiamataPrenotazioniNuove() {
        var loginModel = LoginModel()
        loginModel.uid = user.username
        
        let API = "https://api.spiaggiablu.it/spiaggiablu/protected/listReservationAfterNow"
        let headers: HTTPHeaders = [
            "auth": "\(user.auth as! String)"]
        
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(loginModel)
        let param = (String(data: data, encoding: .utf8))
        let parammm = convertToDictionary(text: param!)
             
             AF.request(API, method: .post, parameters: parammm, encoding: JSONEncoding.default, headers: headers)
                 .validate()
                 .responseJSON { response in
                     
                     switch (response.result) {
                         
                     case .success( _):
                         
                         do {
                            
                            self.prenotazioniNuove = try JSONDecoder().decode([Prenotazione].self, from: response.data!)
                            self.prenotazioniTableView.reloadData()
                            
                            print("SUCCESSO \(self.prenotazioniNuove.count)")
                         }
                         catch let error as NSError {
                             print(error.localizedDescription)
                         }
                     case .failure(let error):
                         print(error.localizedDescription)
                    }
        }
    }
    
    
    func chiamataPrenotazioniPassate() {
        var loginModel = LoginModel()
        loginModel.uid = user.username
        
        let API = "https://api.spiaggiablu.it/spiaggiablu/protected/listReservationBeforeNow"
        let headers: HTTPHeaders = [
            "auth": "\(user.auth as! String)"]
        
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(loginModel)
        let param = (String(data: data, encoding: .utf8))
        let parammm = convertToDictionary(text: param!)
        
               AF.request(API, method: .post, parameters: parammm, encoding: JSONEncoding.default, headers: headers)
                   .validate()
                   .responseJSON { response in
                       
                       switch (response.result) {
                           
                       case .success( _):
                           
                           do {
                              
                              self.prenotazioniPassate = try JSONDecoder().decode([Prenotazione].self, from: response.data!)
                            self.prenotazioniTableView.reloadData()
                           
                            print("PASSATEEEE \(self.prenotazioniPassate.count)")
                        
                           }
                           catch let error as NSError {
                               print(error.localizedDescription)
                           }
                       case .failure(let error):
                           print(error.localizedDescription)
                      }
          }
      }
      
    func createDataString(dataString: String) -> String {
        let months = ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"]
        let anno = String(dataString.prefix(4))
       let start = dataString.index(dataString.startIndex, offsetBy: 5)
        let end = dataString.index(dataString.endIndex, offsetBy: -3)
        let range = start..<end
        let mySubstring = dataString[range]
        var giorno = String(mySubstring)
        let myString = giorno
        let myInt = (myString as NSString).integerValue
        let giornoo = months[myInt-1]
        var startGiorno = dataString.index(dataString.startIndex, offsetBy: 8)
        let endGiorno = dataString.index(dataString.endIndex, offsetBy: 0)
        var rangeGiorno = startGiorno..<endGiorno
        var stringaGiorno = dataString[rangeGiorno]
        var numeroGiorno = String(stringaGiorno)
        if numeroGiorno.first == "0" {
        startGiorno = dataString.index(dataString.startIndex, offsetBy: 9)
            rangeGiorno = startGiorno..<endGiorno
            stringaGiorno = dataString[rangeGiorno]
            numeroGiorno = String(stringaGiorno)
            
        }
       
        
        return "\(numeroGiorno) \(giornoo) \(anno)"
        
    }
    
    
    func chiamataAlert(point: Point, rcode: String) {
        let API = "\(point.restUrl!)protected/getAppointment/\(rcode)"
                   let headers: HTTPHeaders = [
                       "auth": "\(user.auth as! String)"]
                   
                   AF.request(API, method: .get, encoding: JSONEncoding.default, headers: headers)
                       .validate()
                       .responseJSON { response in
                           
                           switch (response.result) {
                               
                           case .success( _):
                               
                               do {
                                self.prenotazioniResponse = try JSONDecoder().decode([PrenotazioneResponse].self, from: response.data!)
                                
                                self.prenotazioneResponse = self.prenotazioniResponse[0]
                                self.numVisitors.text = "Numero visitatori : \(self.prenotazioneResponse.numVisitors!)"
                                self.inData.text = "In data \(self.createDataString(dataString: self.prenotazioneResponse.dateReservation!)) hai effettuato una prenotazione per i seguenti servizi:"
                                
                                self.alertTableView.dataSource = self
                                self.alertTableView.delegate = self
                                self.alertTableView.reloadData()
                             
                                
                            
                               }
                               catch let error as NSError {
                                   print(error.localizedDescription)
                               }
                           case .failure(let error):
                               print(error.localizedDescription)
                          }
              }
        
    }
    
    
}


