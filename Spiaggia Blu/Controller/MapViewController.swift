//
//  MapRistViewController.swift
//  Sooneat
//

import UIKit
import MapKit
import CoreLocation
import Firebase
import Alamofire

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var searchField: UISearchBar!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var searchBarHairStylist: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let span = MKCoordinateSpan()
    var point: Point!
    let API = "https://api.spiaggiablu.it/spiaggiablu/public/pointByDistance"
    var points = [Point]()
    var locationManager = CLLocationManager()
    let authorizationStatus = CLLocationManager.authorizationStatus()
    let regionRadius: Double = 30000
    var currentLocation: CLLocation?
    let userID = Auth.auth().currentUser!.uid
    var location = CLLocation()
    var user: User!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let noLocation = CLLocationCoordinate2D()
        zoomAndCenter(on: noLocation, zoom: 10.0)
        searchField.delegate = self
        let nib = UINib(nibName: "CollectionViewCell", bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: "CellPoint")
        setSearchBarIconColor()
        searchBarShadow()
        mapView.delegate = self
        locationManager.delegate = self
    }

    
    func zoomAndCenter(on centerCoordinate: CLLocationCoordinate2D, zoom: Double) {
        var span: MKCoordinateSpan = mapView.region.span
        span.latitudeDelta *= zoom
        span.longitudeDelta *= zoom
        let region: MKCoordinateRegion = MKCoordinateRegion(center: centerCoordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    @IBAction func esci(_ sender: Any) {
       try! Auth.auth().signOut()
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    @IBAction func unwindToViewControllerA(segue: UIStoryboardSegue) {}
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    if let location = locations.last{
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.mapView.setRegion(region, animated: true)
        }
    }

     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          self.view.endEditing(true)
      
          return true
      }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchField.resignFirstResponder()
       }
    
    func setSearchBarIconColor() {
        if let textFieldInsideSearchBar = self.searchBarHairStylist.value(forKey: "searchField") as? UITextField,
        let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
            glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
            glassIconView.tintColor = .black
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = NSAttributedString(string: "CERCA IL TUO LIDO", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            searchBarHairStylist.barTintColor = .white
            searchBarHairStylist.searchTextField.backgroundColor = .white
        }
    }
    
    
    func searchBarShadow() {
        searchBarHairStylist.layer.shadowOffset = .zero
        searchBarHairStylist.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        searchBarHairStylist.layer.shadowOpacity = 0.2
        searchBarHairStylist.layer.shadowRadius = 7
        searchBarHairStylist.layer.shadowPath = UIBezierPath(rect: searchBarHairStylist.bounds).cgPath
    }
    

    
    override func viewDidAppear(_ animated: Bool) {
   centerMapOnUserLocation()
        loadPoint(ricerca: "")
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
           
       }

    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        searchBarHairStylist.resignFirstResponder()
    }
    

    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("CERCOOOOOOO \(searchBar.text!)")
        loadPoint(ricerca: searchBar.text!)
        searchBarHairStylist.resignFirstResponder()
       }
    


    func centerMapOnUserLocation() {
        guard let coordinate = locationManager.location?.coordinate else {return}
        let coordinateRegion = MKCoordinateRegion(center: coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }

    func configureLocationServices() {
        if authorizationStatus == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        } else {
            return
        }
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        centerMapOnUserLocation()
       
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK - CLLocationManagerDelegate
    
    

    
   /* func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations.last }
        
        if currentLocation == nil {
            // Zoom to user location
            if let userLocation = locations.last {
                let viewRegion = MKCoordinateRegion(center: userLocation.coordinate, latitudinalMeters: 2000, longitudinalMeters: 2000)
                mapView.setRegion(viewRegion, animated: false)
            }
        }
    }*/
    
    func loadPoint(ricerca: String) {
        //CHIAMATA SERVIZIO
        
        let coordinate = CoordinateReq.init(name: ricerca, lat: self.location.coordinate.latitude, lng: self.location.coordinate.longitude, size: 10, page: 1, maxDistance: 20000000)
        
        AF.request(self.API, method: .post, parameters: coordinate.toDict() ,encoding: JSONEncoding.default, headers: nil)
        .validate()
        .responseJSON { response in
            switch (response.result) {
                case .success( _):
                    do {
                        self.points = try JSONDecoder().decode([Point].self, from: response.data!)
                        self.collectionView.reloadData()
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
             }
        }
    }
    
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let customAnnotation = annotation as? MyCustomPointAnnotation {
            var anView = mapView.dequeueReusableAnnotationView(withIdentifier: customAnnotation.title!)

            if anView == nil {
                anView = MKAnnotationView(annotation: annotation, reuseIdentifier: customAnnotation.title)

                anView?.isEnabled = true
                anView?.isSelected = true
                anView?.canShowCallout = false
                anView!.image = UIImage(named:"marker20Blu")
                
            } else {
                // 6
                anView!.annotation = annotation
            }
            
            return anView
        }
        return nil;
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView){
        //CLICK POINT
        print("click point")
    }
    
    // MARK Collection View
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.points.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellPoint", for: indexPath) as! CollectionViewCell
        let point = self.points[indexPath.row]
        cell.nome.text = point.name
        cell.dettagliButtonOutlet.tag = indexPath.row
         cell.dettagliButtonOutlet.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)
        
        cell.indirizzo.text = point.address
        cell.immagine!.downloadImageFrom(link: point.image, contentMode: .scaleAspectFit)
        let annotation = MyCustomPointAnnotation(customVariable: true, index: indexPath.row + 1)
        annotation.coordinate = CLLocationCoordinate2D(latitude: (point.latitude as NSString).doubleValue, longitude: (point.longitude as NSString).doubleValue)
        annotation.title = point.name
        mapView.addAnnotation(annotation)
     
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let point = self.points[indexPath.row]
        self.performSegue(withIdentifier: "aPrenota", sender: point)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    @objc func connected(sender: UIButton){
        let buttonTag = sender.tag
        point = points[buttonTag]
        performSegue(withIdentifier: "aPrenota", sender: self)
    }

    
   /* func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (collectionView == self.collectionViewPromo) {
             return CGSize(width: view.frame.width, height: collectionView.frame.height )
        }
        return CGSize(width: collectionView.frame.width - 50, height: collectionView.frame.height )
    }*/
    
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
  
return CGSize(width: 200, height: 90)
    
}
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: (Any)?) {
       if segue.destination is PrenotazioneViewController
               {
                   let vc = segue.destination as? PrenotazioneViewController
                   vc?.user = self.user
                vc?.point = self.point
                
       } else if segue.destination is ListaPrenotazioniViewController {
        let vc = segue.destination as? ListaPrenotazioniViewController
        vc?.user = self.user
        vc?.point = self.point
        }
    }
        
    }



