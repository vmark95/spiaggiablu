//
//  PrenotazioneViewController.swift
//  Hair Stylist
//
//  Created by Marco Vastolo on 15/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import UIKit
import Alamofire
import Firebase


class PrenotazioneViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    

    @IBOutlet weak var immagineAlertPrenota: UIImageView!
    @IBOutlet weak var immagineAlertConferma: UIImageView!
    @IBOutlet weak var nessunServizioDisponibileLabel: UILabel!
    @IBOutlet weak var spinner: SpinnerLogin!
    @IBOutlet weak var numeroTelefonoTextField: UITextField!
    @IBOutlet weak var prenotaFinaleOutlet: UIButton!
    @IBOutlet weak var viewAlertPrenota: UIView!
    @IBOutlet weak var prenotaArancioneOutlet: UIButton!
    @IBOutlet weak var alertPrenotazioneGrigio: UIView!
    @IBOutlet weak var numVisitorsPicker: UIPickerView!
    @IBOutlet weak var alertTableView: UITableView!
    @IBOutlet weak var spinnerPrenota: SpinnerLogin!
    @IBOutlet weak var numeroTextField: UITextField!
    @IBOutlet weak var prenotazioneOre: UILabel!
    @IBOutlet weak var viewGrigia: UIView!
    @IBOutlet weak var prenotaOutlet: UIButton!
    @IBOutlet weak var serviziPrenotatiTableView: UITableView!
    @IBOutlet weak var logoAlert: UIImageView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var spiacentiOutlet: UILabel!
    @IBOutlet weak var calendarCV: UICollectionView!
    @IBOutlet weak var orarioCV: UICollectionView!
    @IBOutlet weak var prezzoServizio: UILabel!
    @IBOutlet weak var immagineStaffOutlet: UIImageView!
    @IBOutlet weak var nomeStaffOulet: UILabel!
    @IBOutlet weak var checkStatus: UIImageView!
    @IBOutlet weak var servizioLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var nomeBarbiere: UILabel!
    @IBOutlet weak var pickerQuantità: UIPickerView!
    @IBOutlet weak var prenotazioneServiziTableView: UITableView!
    @IBOutlet weak var prenotazioneGiorno: UILabel!
    
    var maxQuantitaCell = 0
    var rcode = ""
    var prenotazioneResponse: PrenotazioneResponse!
    var numeroVisitors = 1
    var quantitàSelezionata = 1
    var delegateOre = ""
    var availabilitySelected: Availability!
    var availabilityReq = AvailabilityReq()
    var availabilityList = [Availability]()
    var user: User!
    var point: Point!
    var dataReservacion = [Date]()
    var servicePreferences = [ServicePreference]()
    let today = Date()
    let dateFormatter = DateFormatter()
    var calendar = Calendar.current
    var month = ""
    var day = ""
    var weekday = ""
    var indice = 0
    var delegato = StaffTableViewCell()
    var dataSelezionata: Date!
    var currentDates = [Date]()
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 20))
    let image = UIImage(systemName: "phone.circle")
    var numeroIndice = 0
    var availabilityDelegate: Availability!
    var serviziSelect = [String]()
    var delegatoAvReq: AvailabilityReq!
    var quantità = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
    var serviceList = [Service]()
    var servicePreference = ServicePreference()
    var indiceServizio = 0
    var serviziPrenotati = [Service]()
    var appointmentReq = AppointmentReq()
    var delegatoPrenotazioneResponse : PrenotazioneResponse!


    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        imageView.tintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        numeroTelefonoTextField.leftViewMode = .always
        imageView.image = image
        numeroTelefonoTextField.leftView = imageView
        
        immagineAlertPrenota.layer.cornerRadius = 15
        immagineAlertConferma.layer.cornerRadius = 15
        
        spinnerPrenota.hidesWhenStopped = true
        numeroTelefonoTextField.delegate = self
        spinner.hidesWhenStopped = true
   
        
        prenotaFinaleOutlet.isEnabled = false
        viewAlertPrenota.isHidden = true
        numVisitorsPicker.selectRow(0, inComponent: 0, animated: true)
        numVisitorsPicker.setValue(UIColor.white, forKey: "textColor")
        prenotaArancioneOutlet.isEnabled = false
        if prenotaArancioneOutlet.isEnabled {
            prenotaArancioneOutlet.backgroundColor = #colorLiteral(red: 0.7934576869, green: 0.528826952, blue: 0, alpha: 1)
        } else {
            prenotaArancioneOutlet.backgroundColor = #colorLiteral(red: 0.7934576869, green: 0.528826952, blue: 0, alpha: 0.4204302226)
        }

        
        alertPrenotazioneGrigio.isHidden = true
        alertTableView.tag = 2
        alertTableView.dataSource = nil
        alertTableView.delegate = self
        numVisitorsPicker.dataSource = nil
        numVisitorsPicker.delegate = self
        numVisitorsPicker.tag = 2
        pickerQuantità.tag = 1
        pickerQuantità.selectRow(0, inComponent: 0, animated: false)
        pickerQuantità.setValue(UIColor.white, forKey: "textColor")
        pickerQuantità.dataSource = nil
        pickerQuantità.delegate = self
        imageView.tintColor = #colorLiteral(red: 0.2903787196, green: 0.2904333472, blue: 0.2903715074, alpha: 1)
        imageView.image = image
        prenotazioneServiziTableView.tag = 1
        alertView.isHidden = true
        logoAlert.layer.cornerRadius = 10
        calendarCV.tag = 2
        calendarCV.delegate = self
        calendarCV.dataSource = self
        nomeBarbiere.text = point.name
        
        alertTableView.register(UINib(nibName: "AlertTableViewCell", bundle: nil), forCellReuseIdentifier: "alertCell")
        
        prenotazioneServiziTableView.tableFooterView = UIView()
        let nib = UINib(nibName: "CalendarioCollectionViewCell", bundle: nil)
        
        calendarCV.register(nib, forCellWithReuseIdentifier: "cellCalendar")
        prenotazioneServiziTableView.register(UINib(nibName: "StaffTableViewCell", bundle: nil), forCellReuseIdentifier: "staffCell")
        
        prenotazioneServiziTableView.estimatedRowHeight = 20
        prenotazioneServiziTableView.rowHeight = UITableView.automaticDimension
        
        prenotazioneServiziTableView.register(UINib(nibName: "PrenotazioneTableViewCell", bundle: nil), forCellReuseIdentifier: "cellaServiziSelezionati")
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        calendarCV.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .left)
        chiamataRestServizi()
    }
    
    
    @IBAction func prenotazioneFinale(_ sender: Any) {
        prenota()
    }
    
    
    @IBAction func prenotaFinale(_ sender: Any) {
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           let text = (numeroTelefonoTextField.text as! NSString).replacingCharacters(in: range, with: string)

        if !text.isEmpty{
            prenotaFinaleOutlet.isUserInteractionEnabled = true
            prenotaFinaleOutlet.isEnabled = true
            prenotaFinaleOutlet.tintColor = .white
            prenotaFinaleOutlet.alpha = 1.0
        } else {
            prenotaFinaleOutlet.isUserInteractionEnabled = false
            prenotaFinaleOutlet.isEnabled = false
            prenotaFinaleOutlet.tintColor = .white
            prenotaFinaleOutlet.alpha = 0.2
        }
           return true
       }
    
    
    @IBAction func prenota(_ sender: Any) {
        alertTableView.dataSource = self
        numVisitorsPicker.dataSource = self
        numVisitorsPicker.selectRow(0, inComponent: 0, animated: true)
        dateFormatter.dateFormat = "dd MMMM yyyy"
        prenotazioneGiorno.text = "PRENOTAZIONE GIORNO\n \(dateFormatter.string(from: dataSelezionata))"
        viewAlertPrenota.isHidden = false
        alertTableView.reloadData()
        alertPrenotazioneGrigio.isHidden = false
    }
    
    
    @IBAction func annullaPrenotazione(_ sender: Any) {
        viewAlertPrenota.isHidden = true
    }
    

    // PICKER VIEW
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            if maxQuantitaCell > 10 {
                return 10
            } else {
                return maxQuantitaCell
            }
        } else {
            return quantità.count
        }
       
       
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return quantità[row]
    }

    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1{
            quantitàSelezionata = row + 1
        } else {
            numeroVisitors = row + 1
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    // PICKER VIEW
    
 
    func recuperoDescrizioneServizi(service: ServicePreference) -> String {
        var indexService = point.service.firstIndex(where: { $0.id == service.service })!
        var descrizioneServizio = point.service[indexService].description
        var quantitaSelezionata = service.quantita
        return "\(quantitaSelezionata!) - \(descrizioneServizio!)"
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        numeroTelefonoTextField.resignFirstResponder()
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    
    func dates(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate
        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
    
   
    func spinnerGo() {
        viewAlertPrenota.isHidden = true
        prenotaArancioneOutlet.isHidden = true
        calendarCV.isHidden = true
        alertPrenotazioneGrigio.isHidden = true
        alertTableView.isHidden = true
        alertView.isHidden = true
        alertTableView.isHidden = true
        prenotazioneServiziTableView.isHidden = true
        spinnerPrenota.startAnimating()
    }
    
    func spinnerStop() {
        viewAlertPrenota.isHidden = false
        prenotaArancioneOutlet.isHidden = false
        calendarCV.isHidden = false
        prenotazioneServiziTableView.isHidden = false
        spinnerPrenota.stopAnimating()
    }
    
    
    func prenota() {
        
        spinnerGo()
        
        let API = "\(point.restUrl!)protected/addAppointment"
        let headers: HTTPHeaders = [
            "auth": "\(user.auth as! String)"]
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        appointmentReq.dateReservation = dateFormatter.string(from: dataSelezionata)
        appointmentReq.clientPhone = numeroTelefonoTextField.text!
        appointmentReq.clientFullname = user.usersDetail.displayName
        appointmentReq.clientUid = user.username
        appointmentReq.servicePreferences = servicePreferences
        appointmentReq.clientEmail = user.usersDetail.mail
        appointmentReq.numVisitors = numeroVisitors
        
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(appointmentReq)
        let param = (String(data: data, encoding: .utf8))
        let parammm = convertToDictionary(text: param!)
        
        AF.request(API, method: .post, parameters: parammm, encoding: JSONEncoding.default, headers: headers)
            .validate()
            .responseJSON { response in
                
                switch (response.result) {
                    
                case .success( _):
                    
                    do {
                        self.prenotazioneResponse = try JSONDecoder().decode(PrenotazioneResponse.self, from: response.data!)
                  
                        self.rcode = self.prenotazioneResponse.rcode!
                        
                        self.performSegue(withIdentifier: "prenotazioneOK", sender: self)
                        
                        self.spinnerStop()
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                        self.spinnerStop()
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    self.spinnerStop()
            }
        }
    }
    
    
    @IBAction func annulla(_ sender: Any) {
        alertView.isHidden = true
    }
    
    
    @IBAction func conferma(_ sender: Any) {
        
        
        point.service[indiceServizio].checked = true
        servicePreference.service = point.service[indiceServizio].id
        servicePreference.quantita = quantitàSelezionata
        servicePreferences.append(servicePreference)
        alertTableView.reloadData()
        alertView.isHidden = true
        
        prenotaArancioneOutlet.isEnabled = true
        prenotaArancioneOutlet.backgroundColor = #colorLiteral(red: 0.7934576869, green: 0.528826952, blue: 0, alpha: 1)
        prenotazioneServiziTableView.reloadData()
        quantitàSelezionata = 1
    }
    
    
    func disablePrenota() {
        
        if servicePreferences != nil && servicePreferences.count > 0 {
            prenotaArancioneOutlet.isEnabled = true
            prenotaArancioneOutlet.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.5294117647, blue: 0, alpha: 1)
        } else {
            prenotaArancioneOutlet.isEnabled = false
            prenotaArancioneOutlet.backgroundColor = #colorLiteral(red: 0.7921568627, green: 0.5294117647, blue: 0, alpha: 0.42)
        }
    }
    
    
    // COLLECTION VIEW
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       if collectionView.tag == 2 {
            return 100
        } else if collectionView.tag == 3 {
            return availabilityList.count
        } else {
            return 1
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 1 {
            
        } else if collectionView.tag == 2 {
            disablePrenota()
            dataSelezionata = currentDates[indexPath.item]
            chiamataRestServizi()
            servicePreferences.removeAll()
            
        } else if collectionView.tag == 3 {
            availabilitySelected = availabilityList[indexPath.item]
            if user.usersDetail.phone != nil {
                numeroTextField.text = user.usersDetail.phone
            }
            prenotazioneOre.text = "PRENOTAZIONE ORE \(String(availabilitySelected.start).prefix(5))" // substring
            availabilityDelegate = availabilitySelected
            delegateOre = "\(String(availabilitySelected.start).prefix(5))"
            serviziPrenotatiTableView.reloadData()
            alertView.isHidden = false
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 2 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCalendar", for: indexPath) as! CalendarioCollectionViewCell
            
            let afterThirtyDays = calendar.date(byAdding: .day, value: 100, to: today)
            currentDates = dates(from: today, to: afterThirtyDays!)
            dataSelezionata = today
            dateFormatter.locale = Locale(identifier: "it_IT")
            var currentData = currentDates[indexPath.item]
            dateFormatter.dateFormat = "MMMM"
            month = dateFormatter.string(from: currentData).capitalized
            dateFormatter.dateFormat = "dd"
            day = dateFormatter.string(from: currentData)
            dateFormatter.dateFormat = "EEEE"
            weekday = dateFormatter.string(from: currentData).capitalized
            //dataSelezionata = currentData
            
            cell.giorno.text = String(weekday.prefix(3))   // SOLO PRIME TRE LETTERE
            cell.numeroGiorno.text = day
            cell.mese.text = String(month.prefix(3))       //SOLO PRIME TRE LETTERE
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCalendar", for: indexPath) as! CalendarioCollectionViewCell
            cell.giorno.text = "giorno"
            cell.mese.text = "mese"
            cell.numeroGiorno.text = "\(10)"
            return cell
        }
    }
    // COLLECTION VIEW
    
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    func chiamataRestServizi() {
        spinner.startAnimating()
        if dataSelezionata != nil {
            
            dateFormatter.dateFormat = "YYYY-MM-dd"
            let API = "\(point.restUrl!)protected/getListAvailability"
        
            availabilityReq.dateReservation = dateFormatter.string(from: dataSelezionata)
            
            let headers: HTTPHeaders = [
                "auth": "\(user.auth as! String)"]
            
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let data = try! encoder.encode(availabilityReq)
            let param = (String(data: data, encoding: .utf8))
            let parammm = convertToDictionary(text: param!)
                        
            AF.request(API, method: .post, parameters: parammm, encoding: JSONEncoding.default, headers: headers)
                .validate()
                .responseJSON { response in
                    
                    switch (response.result) {
                        
                    case .success( _):
                        
                        do {
                            self.serviceList = try JSONDecoder().decode([Service].self, from: response.data!)
                            self.point.service = self.serviceList
                            self.prenotazioneServiziTableView.reloadData()
                            self.spinner.stopAnimating()
                            if self.point.service.count == 0 {
                                self.nessunServizioDisponibileLabel.isHidden = false
                            } else {
                                self.nessunServizioDisponibileLabel.isHidden = true
                            }
                        }
                        catch let error as NSError {
                            print(error as! String)
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
            }
        } else {
            return
        }
    }
    
    
    // TABLE VIEW
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            if point.service != nil {
                
                return point.service.count
                
            } else {
                return 0
                
            }
            
        } else if tableView.tag == 2 {
            
            if servicePreferences.count > 0 {
                
                return servicePreferences.count
                
            } else {
                return 0
            }
        } else {
            return 0
        }
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 1 {
        return 50
        } else {
            return 40
        }
    }

        
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 1 {
            
            if point.service[indexPath.row].checked == true {
                point.service[indexPath.row].checked = false
                
                tableView.reloadData()
                
                servicePreferences.remove(at: servicePreferences.firstIndex(where: { $0.service == point.service[indexPath.row].id })!)
        
                disablePrenota()
                
            } else {
                
                maxQuantitaCell = point.service[indexPath.row].quantita!
                indiceServizio = indexPath.row
                pickerQuantità.dataSource = self
                alertView.isHidden = false
                pickerQuantità.selectRow(0, inComponent: 0, animated: true)
                
                
                /* point.service[indexPath.section].checked = true
                 servicePreference.service = point.service[indexPath.section].id
                 servicePreferences.append(servicePreference)*/
            }
        } else if tableView.tag == 2 {
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.tag == 1 {
            
            guard let tableViewCell = cell as? StaffTableViewCell else { return }
            
            tableViewCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
        } else if tableView.tag == 2 {
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellaServiziSelezionati") as! PrenotazioneTableViewCell
            cell.nomeServizio.text = point.service[indexPath.row].description
            cell.prezzo.text = "\(point.service[indexPath.row].price!)0€"
            cell.note.text = point.service[indexPath.row].note
            cell.selectionStyle = .none
            
            if point.service[indexPath.row].checked == false {
                cell.check.isHidden = true
        
            } else {
                cell.check.isHidden = false
            }
                return cell
            
        } else if tableView.tag == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "alertCell") as! AlertTableViewCell
            cell.selectionStyle = .none
            cell.label.text = recuperoDescrizioneServizi(service: servicePreferences[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "alertCell") as! AlertTableViewCell
            cell.selectionStyle = .none
            return cell
        }
    }
    
      
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is OKPrenotazioneViewController {
            let vc = segue.destination as? OKPrenotazioneViewController
            vc?.user = self.user
            vc?.point = self.point
            vc?.ora = delegateOre
            vc?.dataSelected = dataSelezionata
            vc?.avReq = self.delegatoAvReq
            vc?.dataa = self.dataSelezionata
            vc?.rcode = self.rcode
            vc?.appointmentReq = self.appointmentReq
        }
    }

    
    func createDataString(dataString: String) -> String {
           let months = ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"]
           let anno = String(dataString.prefix(4))
          let start = dataString.index(dataString.startIndex, offsetBy: 5)
           let end = dataString.index(dataString.endIndex, offsetBy: -3)
           let range = start..<end
           let mySubstring = dataString[range]
           var giorno = String(mySubstring)
           let myString = giorno
           let myInt = (myString as NSString).integerValue
           let giornoo = months[myInt-1]
           var startGiorno = dataString.index(dataString.startIndex, offsetBy: 8)
           let endGiorno = dataString.index(dataString.endIndex, offsetBy: 0)
           var rangeGiorno = startGiorno..<endGiorno
           var stringaGiorno = dataString[rangeGiorno]
           var numeroGiorno = String(stringaGiorno)
           if numeroGiorno.first == "0" {
           startGiorno = dataString.index(dataString.startIndex, offsetBy: 9)
               rangeGiorno = startGiorno..<endGiorno
               stringaGiorno = dataString[rangeGiorno]
               numeroGiorno = String(stringaGiorno)
               
           }
          
           
           return "\(numeroGiorno) \(giornoo) \(anno)"
           
       }
       
    
}

