//
//  DetailViewController.swift
//  Hair Stylist
//
//  Created by Salvatore Serafino on 10/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import UIKit
import Alamofire

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var foto: UIImageView!
    @IBOutlet weak var serviziTableView: UITableView!
    @IBOutlet weak var descrizione: UILabel!
    
    var point : Point!
    var user: User!
    var service = [Service]()
    let defaults = UserDefaults.standard
    
    
    override func viewDidLoad() {
           super.viewDidLoad()
           serviziTableView.register(UINib(nibName: "BarbieriTableViewCell", bundle: nil), forCellReuseIdentifier: "cellaServizi")
           self.fetch()
           // Do any additional setup after loading the view.
           self.nome.text = point.name
           self.address.text = point.address
           serviziTableView.tableFooterView = UIView()
           serviziTableView.rowHeight = 40
          // self.foto!.downloadImageFrom(link: point.image, contentMode: .scaleAspectFit)
       }
       
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return service.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellaServizi") as! BarbieriTableViewCell
        cell.prezzoOutlet.text = "\(service[indexPath.row].price!)0€"
        cell.servizioOutlet.text = service[indexPath.row].description
        return cell
    }

   
    func fetch() {
        let API = "\(point.restUrl!)protected/getService/enabled"
        let headers: HTTPHeaders = [
       "auth": "\(user.auth as! String)"]
        
        AF.request(API, method: .get, encoding: JSONEncoding.default, headers: headers)
        .validate()
        .responseJSON { response in
            switch response.result {
                
                case .success( _):
                    
                    do {
                        self.service = try JSONDecoder().decode([Service].self, from: response.data!)
                        var index = 0
                        var staffElement = StaffDetail(id: 0, uid: "", fullname: "Chiunque", image: "https://i.pinimg.com/originals/18/91/57/189157674247497178577a9f7599377d.jpg", tokenMobile: "", enabled: true)
                      
                    
                        
                        self.point.service = self.service
                        
                    
                            self.serviziTableView.reloadData()
                        
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                break
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    // number of rows in table view
    /*func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return
    }

    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellService", for: indexPath) as! CellService
        
        let servizio: Service = self.point.service[indexPath.row]
        
        cell.servizio.text = servizio.description
        cell.prezzo.text = String(servizio.price)
        
        return cell
    }

    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    */
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if segue.destination is PrenotazioneViewController {
            let vc = segue.destination as? PrenotazioneViewController
            vc?.user = self.user
            vc?.point = self.point
        }
    }
    
    
}
let imageCache = NSCache<AnyObject, AnyObject>()
extension UIImageView {
    
    func downloadImageFrom(link:String, contentMode: UIView.ContentMode) {
        if let imageFromCache = imageCache.object(forKey: link as AnyObject) as? UIImage {
            self.image = imageFromCache
        } else {
            URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
                (data, response, error) -> Void in
                if data != nil {
                    DispatchQueue.main.async {
                        self.contentMode =  contentMode
                        let imageToCache = UIImage(data: data!)
                        if imageToCache != nil {
                            imageCache.setObject(imageToCache!, forKey: link as AnyObject)
                            self.image = imageToCache
                        } else {
                            self.image = UIImage(named: "iconalista")
                        }
                    }
                }
            }).resume()
        }
    }
}
