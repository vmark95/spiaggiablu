//
//  HomeViewController.swift
//  Sooneat
//


import UIKit
import Firebase
import GoogleSignIn
import FBSDKLoginKit
import Alamofire


class HomeViewController: UIViewController, LoginButtonDelegate, GIDSignInDelegate {
    
    @IBOutlet weak var facebookOutlet: UIButton!
    @IBOutlet weak var twitterOutlet: UIButton!
    @IBOutlet weak var spinner: SpinnerLogin!
    @IBOutlet var txtCondizioni: UITextView!
    @IBOutlet weak var imageLogin: UIImageView!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var googleOutlet: UIButton!
    
    var user: User!
    let defaults = UserDefaults.standard
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        spinner.hidesWhenStopped = true
        configureGoogleSignInButton()
        configureFacebookSignInButton()
 
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
          if (error) != nil {
              print("An error occured during Google Authentication")
              return
          }

          guard let authentication = user.authentication else { return }
          let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                         accessToken: authentication.accessToken)
          Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
              if (error) != nil {
                  print("Google Authentification Fail")
              } else {
                  print("Google Authentification Success")
                  let userID = Auth.auth().currentUser!.uid
                self.logIn(uid: userID)
                  
                  
                  
                  
                  //TODO CALL E AUTOOK
                  
                  return
              }
          }
      }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
           let firebaseAuth = Auth.auth()
           do {
               try firebaseAuth.signOut()
           } catch let signOutError as NSError {
               print ("Error signing out: %@", signOutError)
           }
       }
    
   /* @IBAction func twitterSign(_ sender: Any) {
        TWTRTwitter.sharedInstance().logIn {
            (session, error) -> Void in
            if (error != nil) {
                print("Twitter authentication failed PRIMO")
            } else {
                guard let token = session?.authToken else {return}
                guard let secret = session?.authTokenSecret else {return}
                let credential = TwitterAuthProvider.credential(withToken: token, secret: secret)
                Auth.auth().signIn(with: credential, completion: { (user, error) in
                    if error == nil {
                        print("Twitter authentication succeed 200")
                        let userID = Auth.auth().currentUser!.uid
                        print(userID)
                        //TODO Call Service Login
                        
                        self.logIn(uid: Auth.auth().currentUser!.uid)
                    } else {
                        print("Twitter authentication failed SECONDO")
                    }
                })
            }
        }
    }
    */
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "authOK"  {
            
            if let navController = segue.destination as? UINavigationController {
                
                if let chidVC = navController.topViewController as? MapViewController {
                    chidVC.user = self.user
                }
                
            }
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        if Auth.auth().currentUser != nil {
            logIn(uid: Auth.auth().currentUser!.uid)
        } else {
            return
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //This is for the keyboard to GO AWAYY !! when user clicks anywhere on the view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func login(_ sender: Any) {
        self.view.endEditing(true)
        Auth.auth().signIn(withEmail: emailText.text!, password: passwordText.text!, completion: { (user, error) in
            if user != nil {
                //Sign in successful
                self.performSegue(withIdentifier: "authOK", sender: self)
            } else {
                if let myError = error?.localizedDescription {
                    self.errorLabel.text = myError
                } else {
                    self.errorLabel.text = NSLocalizedString("GENERIC_ERROR", comment: "")
                }
            }
        })
    }
    
    //creating the Google sign in button
    fileprivate func configureGoogleSignInButton() {
        let googleSignInButton = GIDSignInButton()
        googleSignInButton.frame = CGRect(x: 120, y: 200, width: view.frame.width - 240, height: 50)
        //view.addSubview(googleSignInButton)
        GIDSignIn.sharedInstance()?.presentingViewController = self
        //GIDSignIn.sharedInstance().signIn()
    }
    
    //creating the Facebook sign in button
    fileprivate func configureFacebookSignInButton() {
        let facebookSignInButton = FBLoginButton()
        facebookSignInButton.frame = CGRect(x: 120, y: 200 + 100, width: view.frame.width - 240, height: 40)
        //view.addSubview(facebookSignInButton)
        facebookSignInButton.delegate = self
    }
    
    
    
    //FBSDKLoginButton delegate methods
    func loginButton(_ loginButton: FBLoginButton!, didCompleteWith result: LoginManagerLoginResult!, error: Error!) {
        if error == nil {
            print("User just logged in via Facebook 95")
        } else {
            print("An error occured the user couldn't log in")
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        print("User just logged out from his Facebook account")
    }
    
    //creating the Twitter sign in button
    /*fileprivate func configureTwitterSignInButton() {
        let twitterSignInButton = TWTRLogInButton(logInCompletion: { session, error in
            if (error != nil) {
                print("Twitter authentication failed")
            } else {
                print("Twitter authentication succeed 142")
            }
        })
        
        twitterSignInButton.frame = CGRect(x: 120, y: 400, width: view.frame.width - 240, height: 40)
        //self.view.addSubview(twitterSignInButton)
    }
    */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    @IBAction func googleSignIn(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
        
    }
    
    
    @IBAction func twitterSignIn(_ sender: UIButton) {
        
    }
    
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    func spinnerGo() {
        spinner.startAnimating()
        self.googleOutlet.isHidden = true
        self.facebookOutlet.isHidden = true
    }
    
    func spinnerStop() {
        spinner.stopAnimating()
        self.googleOutlet.isHidden = false
        self.facebookOutlet.isHidden = false
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        spinnerStop()
    }
    
    
    func logIn(uid: String) {
        
        
        print("UIDDD \(uid)  UIDDDDD")
        self.spinnerGo()
        var loginModel = LoginModel()
        loginModel.uid = uid
        let API = "https://api.spiaggiablu.it/spiaggiablu/public/login"
        
        
        
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(loginModel)
        let param = (String(data: data, encoding: .utf8))
        let parammm = convertToDictionary(text: param!)
        
        
        // let jsonString = String(data: json, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) as! Dictionary<String, Any>
        
        
        AF.request(API, method: .post, parameters: parammm, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                
                switch (response.result) {
                    
                case .success( _):
                    
                    do {
                        
                        self.user = try JSONDecoder().decode(User.self, from: response.data!)
                        self.user.auth = response.response?.allHeaderFields["auth"] as! String
                        
                        self.performSegue(withIdentifier: "authOK", sender: self)
                      
                        
                    }
                    catch let error as NSError {
                        print(error as! String)
                        self.spinnerStop()
                    }
                    
                case .failure(let error):
                    print(error.localizedDescription)
                    self.spinnerStop()
                }
        }
    }
    
    
    
    @IBAction func loginFacebook(_ sender: Any) {
        
        LoginManager().logIn(permissions: ["email"], from: self) { (result, error) in
            if error == nil {
                print("User just logged in via Facebook")
                let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
                Auth.auth().signIn(with: credential, completion: { (user, error) in
                    if (error != nil) {
                        print("Facebook authentication failed")
                    } else {
                        print("Facebook authentication succeed 235")
                        
                        if user != nil {
                            //User Created
                            let userID = Auth.auth().currentUser!.uid
                            print(userID)
                            self.logIn(uid: userID)
                            
                        }
                    }
                })
            } else
            {
                
                print("An error occured the user couldn't log in")
            }
        }
    }
}
