//
//  AppointmentReq.swift
//  SpiaggiaBlu
//
//  Created by Marco Vastolo on 29/05/2020.
//  Copyright © 2020 Gecotech Srl. All rights reserved.
//

import Foundation

struct AppointmentReq: Codable {
   
    var dateReservation: String!
    var clientUid: String!
    var clientFullname: String!
    var clientPhone: String?
    var clientEmail: String?
    var numVisitors: Int!
    var servicePreferences: [ServicePreference]?
 
    
    enum CodingKeys: String, CodingKey {
       case dateReservation = "dateReservation"
       case clientUid = "clientUid"
       case clientFullname = "clientFullname"
       case clientPhone = "clientPhone"
       case clientEmail = "clientEmail"
       case numVisitors = "numVisitors"
       case servicePreferences = "servicePreferences"
    }
   /* init(dataReservation: String,
          clientFullname: String,
          clientPhone: String,
          clientEmail: String,
          clientUid: String,
          numVisitors: Int,
          servicePreferences: [ServicePreference]
       
      ) {
        self.clientUid = clientUid
        self.clientFullname = clientFullname
        self.clientPhone = clientPhone
        self.dataReservation = dataReservation
        self.numVisitors = numVisitors
        self.servicePreferences = servicePreferences
        self.clientEmail = clientEmail
      }*/
}
