//
//  SpinnerLogin.swift
//  Hair Stylist
//
//  Created by Marco Vastolo on 23/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import UIKit

class SpinnerLogin: UIActivityIndicatorView {

    
    override func draw(_ rect: CGRect) {
        let arc = UIBezierPath(arcCenter: CGPoint(x: self.frame.size.width/2 ,y: self.frame.size.height/2), radius: CGFloat(self.frame.size.height/4.5), startAngle: CGFloat(Double.pi), endAngle:CGFloat(Double.pi*3/2), clockwise: false)
        arc.lineCapStyle = .round
        #colorLiteral(red: 0.7934576869, green: 0.528826952, blue: 0, alpha: 1).setStroke()
        arc.lineWidth = 3
        arc.stroke()
    
        rotate()
    }
    
    func rotate() {
    let rotation: CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
      rotation.toValue = Double.pi * 2
      rotation.duration = 1 // or however long you want ...
      rotation.isCumulative = true
      rotation.repeatCount = Float.greatestFiniteMagnitude
      self.layer.add(rotation, forKey: "rotationAnimation")
    }


}
