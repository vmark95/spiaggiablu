//
//  Availability.swift
//  Hair Stylist
//
//  Created by Marco Vastolo on 20/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import Foundation

struct AvailabilityReq: Codable {
    
    var id: Int?
    var dateReservation: String?
    var start: Date?
    var end: Date?
    var clientUid: String?
    var clientFullname: String?
    var clientPhone: String?
    var clientEmail: String?
    var clientTokenMobile: String?
    var status: Int?
    var description: String?
    var idService: Int?
    var idStaff: Int?
    var totSlot: Int?
    var servicePreferences: [ServicePreference]?
    var selectedAvailability: Availability?
    var numVisitors: Int!
    
    
    
  enum CodingKeys: String, CodingKey {
    case id = "id"
    case dateReservation = "dateReservation"
    case start = "start"
    case end = "end"
    case clientUid = "clientUid"
    case clientFullname = "clientFullname"
    case clientPhone = "clientPhone"
    case clientEmail = "clientEmail"
    case clientTokenMobile = "clientTokenMobile"
    case status = "status"
    case description = "description"
    case idService = "idService"
    case idStaff = "idStaff"
    case totSlot = "toSlot"
    case servicePreferences = "servicePreferences"
    case selectedAvailability = "selectedAvailability"
    case numVisitors = "numVisitors"
        
       }
}
