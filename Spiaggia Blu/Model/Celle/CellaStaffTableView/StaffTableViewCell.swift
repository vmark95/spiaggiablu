//
//  StaffTableViewCell.swift
//  Hair Stylist
//
//  Created by Marco Vastolo on 19/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import UIKit

class StaffTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionV: CollectionViewSTaff!
    
    
    

   var sezione = 0
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      

        
    }
    
    


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        // Configure the view for the selected state
    }
}




extension StaffTableViewCell {
    
   /* func setCollectioViewDataAndDelegate
        <D: UICollectionViewDelegate & UICollectionViewDataSource>
        (_ dataSourceDelegate: D, forRow row: Int)
    {
        collectionV.delegate = dataSourceDelegate
        collectionV.dataSource = dataSourceDelegate
        collectionV.reloadData()
    }*/
    
    func setCollectionViewDataSourceDelegate(dataSourceDelegate: UICollectionViewDataSource & UICollectionViewDelegate, forRow row: Int) {
        collectionV.delegate = dataSourceDelegate
        collectionV.dataSource = dataSourceDelegate
        collectionV.tag = row
        collectionV.reloadData()
        
    }
}
