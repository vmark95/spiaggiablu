//
//  ListaPrenotazioniCellaTableViewCell.swift
//  SpiaggiaBlu
//
//  Created by Marco Vastolo on 01/06/2020.
//  Copyright © 2020 Gecotech Srl. All rights reserved.
//

import UIKit

class ListaPrenotazioniCellaTableViewCell: UITableViewCell {

    @IBOutlet weak var codicePrenotazione: UILabel!
    @IBOutlet weak var data: UILabel!
    @IBOutlet weak var nomeLido: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
