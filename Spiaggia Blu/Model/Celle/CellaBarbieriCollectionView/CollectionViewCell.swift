//
//  CollectionViewCell.swift
//  Hair Stylist
//
//  Created by Marco Vastolo on 14/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var nome: UILabel!
    
    @IBOutlet weak var dettagliButtonOutlet: UIButton!
    @IBOutlet weak var indirizzo: UILabel!
    @IBOutlet weak var immagine: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
