//
//  PrenotazioneTableViewCell.swift
//  Hair Stylist
//
//  Created by Marco Vastolo on 15/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import UIKit

class PrenotazioneTableViewCell: UITableViewCell {

    @IBOutlet weak var nomeServizio: UILabel!
    @IBOutlet weak var prezzo: UILabel!
    @IBOutlet weak var note: UILabel!
    @IBOutlet weak var check: UIImageView!
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        //Elimina rettangolo grigio durante seleziona multipla dei servizi
       self.selectedBackgroundView = UIView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}
