//
//  BarbieriTableViewCell.swift
//  Hair Stylist
//
//  Created by Marco Vastolo on 15/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import UIKit

class BarbieriTableViewCell: UITableViewCell {

    @IBOutlet weak var prezzoOutlet: UILabel!
    @IBOutlet weak var servizioOutlet: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
