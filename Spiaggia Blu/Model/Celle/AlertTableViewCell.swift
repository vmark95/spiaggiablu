//
//  AlertTableViewCell.swift
//  SpiaggiaBlu
//
//  Created by Marco Vastolo on 29/05/2020.
//  Copyright © 2020 Gecotech Srl. All rights reserved.
//

import UIKit

class AlertTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
