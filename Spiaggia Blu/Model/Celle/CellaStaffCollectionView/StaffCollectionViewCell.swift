//
//  StaffCollectionViewCell.swift
//  Hair Stylist
//
//  Created by Marco Vastolo on 19/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import UIKit

class StaffCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var immagineStaff: UIImageView!
    @IBOutlet weak var nomeStaff: UILabel!
    
    
    
}
