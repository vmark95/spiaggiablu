//
//  PrenotazioniEffettuateTableViewCell.swift
//  SpiaggiaBlu
//
//  Created by Marco Vastolo on 02/06/2020.
//  Copyright © 2020 Gecotech Srl. All rights reserved.
//

import UIKit

class PrenotazioniEffettuateTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelDescrizione: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
