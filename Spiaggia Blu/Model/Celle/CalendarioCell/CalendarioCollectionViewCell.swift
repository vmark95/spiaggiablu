//
//  CalendarioCollectionViewCell.swift
//  Hair Stylist
//
//  Created by Marco Vastolo on 19/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import UIKit

class CalendarioCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewSelezionato: UIView!
    @IBOutlet weak var giorno: UILabel!
    @IBOutlet weak var mese: UILabel!
    @IBOutlet weak var numeroGiorno: UILabel!
    
    var selezionato: Bool = false
    
    override var isSelected: Bool {
    didSet{
        if self.isSelected
        {
            super.isSelected = true
            viewSelezionato.isHidden = false
        }
        else
        {
            super.isSelected = false
            viewSelezionato.isHidden = true
        }
    }
    
   // override func awakeFromNib() {
       // super.awakeFromNib()
        // Initialization code

    }

}

