//
//  ServiziPrenotatiTableViewCell.swift
//  Hair Stylist
//
//  Created by Marco Vastolo on 23/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import UIKit

class ServiziPrenotatiTableViewCell: UITableViewCell {

    @IBOutlet weak var serviziPrenotati: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
