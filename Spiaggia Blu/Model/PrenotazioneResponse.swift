//
//  PrenotazioneResponse.swift
//  SpiaggiaBlu
//
//  Created by Marco Vastolo on 01/06/2020.
//  Copyright © 2020 Gecotech Srl. All rights reserved.
//

import Foundation

struct PrenotazioneResponse: Decodable {
    
    var numVisitors: Int?
    var idAppointment: Int?
    var clientFullname: String?
    var clientEmail: String?
    var clientPhone: String?
    var dateReservation: String?
    var rcode: String?
    var serviceResponses: [Service]?
    
    
    enum CodingKeys: String, CodingKey {
        case numVisitors = "numVisitors"
        case idAppointment = "idAppointment"
        case clientFullname = "clientFullname"
        case clientEmail = "clientEmail"
        case clientPhone = "clientPhone"
        case dateReservation = "dateReservation"
        case rcode = "rcode"
        case serviceResponses = "serviceResponses"
    }
    
   /* init(dateReservation: String,
             clientFullname: String,
             rcode: String,
             idAppointment: Int,
             clientPhone: String,
             clientEmail: String,
             numVisitors: Int,
             serviceResponses: [Service]
          
         ) {
        
           self.idAppointment = idAppointment
           self.rcode = rcode
           self.clientFullname = clientFullname
           self.clientPhone = clientPhone
           self.dateReservation = dateReservation
           self.numVisitors = numVisitors
           self.serviceResponses = serviceResponses
           self.clientEmail = clientEmail
         }*/
    
}
