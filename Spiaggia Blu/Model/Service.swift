//
//  Service.swift
//  Hair Stylist
//
//  Created by Salvatore Serafino on 04/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import Foundation

struct Service: Codable {
    var id: Int!
    var description: String!
   // var minute: Int!
    //var priority: Int!
    var price: Double!
    var enabled: Bool!
    //var staffDetails: [StaffDetail]!
    //var idStaffSelected: Int!
    var checked: Bool? = false
    var quantita: Int?
    var note: String?
    
    
    init(id: Int,
         description: String,
         price: Double,
         enabled: Bool,
         quantita: Int,
         note: String
    ) {
        self.id = id
        self.description = description
        self.price = price
        self.enabled = enabled
        self.note = note
        self.quantita = quantita
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case description = "description"
        case price = "price"
        case enabled = "enabled"
        case note = "note"
        case quantita = "quantita"
    }
}


