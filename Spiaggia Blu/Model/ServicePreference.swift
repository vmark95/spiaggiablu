//
//  ServicePreference.swift
//  Hair Stylist
//
//  Created by Marco Vastolo on 20/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import Foundation


struct ServicePreference: Codable {
    var service: Int?
    var quantita: Int?
    
    
    enum CodingKeys: String, CodingKey {
        case service = "service"
      case quantita = "quantita"
       }
    
    /*init(service: Int,
         staffId: Int,
         priority: Int,
         description: String,
         numSlot: Int,
         start: String,
         end: String) {
        
        self.service = service
        self.staffId = staffId
        self.priority = priority
        self.description = description
        self.numSlot = numSlot
        self.start = start
        self.end = end
    }
*/
}
