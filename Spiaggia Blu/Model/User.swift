//
//  User.swift
//  Hair Stylist
//
//  Created by Salvatore Serafino on 04/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import Foundation

struct User: Decodable {
    let id: Int!
    let enabled: Bool!
    let password: String!
    let username: String!
    var auth: String!
    let usersDetail: UsersDetail!
    
    
    init(id: Int,
        enabled: Bool,
        password: String,
        username: String,
        auth: String,
        usersDetail: UsersDetail
    ) {
        self.id = id
        self.enabled = enabled
        self.password = password
        self.username = username
        self.auth = auth
        self.usersDetail = usersDetail
    }
    
    enum CodingKeys: String, CodingKey {
      case id = "id"
      case enabled = "enabled"
      case password = "password"
      case username = "username"
      case auth = "auth"
      case usersDetail = "usersDetail"
    }
}



