//
//  UserDetail.swift
//  Hair Stylist
//
//  Created by Salvatore Serafino on 04/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import Foundation

struct UsersDetail: Decodable {
    let displayName: String!
    let image: String!
    let mail: String!
    let tokenMobile: String!
    let phone: String?
    
    
    init(displayName: String,
        image: String,
        mail: String,
        tokenMobile: String,
        phone: String
    ) {
        self.displayName = displayName
        self.image = image
        self.mail = mail
        self.tokenMobile = tokenMobile
        self.phone = phone
    }
    
    enum CodingKeys: String, CodingKey {
      case displayName = "displayName"
      case image = "image"
      case mail = "mail"
      case tokenMobile = "tokenMobile"
        case phone = "phone"
    }
}



