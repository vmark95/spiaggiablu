//
//  Prenotazione.swift
//  SpiaggiaBlu
//
//  Created by Marco Vastolo on 01/06/2020.
//  Copyright © 2020 Gecotech Srl. All rights reserved.
//

import Foundation

struct Prenotazione: Decodable {
    var dateReservation: String
    var userUid: String
    var point: Point
    var rCode: String
    
    init(dateReservation: String,
             userUid: String,
             rCode: String,
             point: Point
         ) {
        self.dateReservation = dateReservation
        self.userUid = userUid
        self.rCode = rCode
        self.point = point
    }
    
}
