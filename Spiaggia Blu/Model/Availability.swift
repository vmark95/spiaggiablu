//
//  AvailaibilityReq.swift
//  Hair Stylist
//
//  Created by Marco Vastolo on 20/05/2020.
//  Copyright © 2020 Salvatore Serafino. All rights reserved.
//

import Foundation

struct Availability: Codable {
    var start: String
    var end: String
    var servicePreferences: [ServicePreference]?
    
    
    enum CodingKeys: String, CodingKey {
        
        case start = "start"
        case end = "end"
        case servicePreferences = "servicePreferences"
        
       }
}
