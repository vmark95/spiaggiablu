//
//  MyCustomPointAnnotation.swift
//  Sooneat
//

import UIKit
import MapKit

class MyCustomPointAnnotation: MKPointAnnotation {
    var customVariable: Bool!
    var index: Int!
    
    init(customVariable: Bool, index: Int) {
        self.customVariable = customVariable
        self.index = index
    }
} 
